import {random} from "./helpers/globalHelper";

export function fight(firstFighter, secondFighter) {

    let fistHealth = firstFighter.health;
    let secondHealth = secondFighter.health;
    let winner;

    while (true) {

        secondHealth = secondHealth - getDamage(firstFighter, secondFighter);

        if(secondHealth <= 0){
            winner = firstFighter;
            break;
        }

        fistHealth = fistHealth - getDamage(secondFighter, firstFighter);

        if(fistHealth <= 0){
            winner = secondFighter;
            break;
        }

    }

    return winner;
}

export function getDamage(attacker, enemy) {

    const hitPower = getHitPower(attacker);
    const blockPower = getBlockPower(enemy);

    return blockPower > hitPower ? 0 : hitPower - blockPower;
}

export function getHitPower(fighter) {
    return fighter.attack * random(1, 2);
}

export function getBlockPower(fighter) {
    return fighter.defense * random(1, 2);
}

