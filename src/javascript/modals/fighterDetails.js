import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const { name, attack, defense, health, source } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'div', className: 'fighter-name' });
  const attackElement = createElement({ tagName: 'div', className: 'fighter-detail' });
  const defenseElement = createElement({ tagName: 'div', className: 'fighter-detail' });
  const healthElement = createElement({ tagName: 'div', className: 'fighter-detail' });
  const imageElement = createElement({ tagName: 'img', className: 'fighter-image'});

  nameElement.innerText = name;

  attackElement.innerHTML = "<b>Attack:</b>" + attack;
  defenseElement.innerHTML = "<b>Defense:</b>" + defense;
  healthElement.innerHTML = "<b>Health:</b>" + health;

  imageElement.src = source;

  fighterDetails.append(nameElement);
  fighterDetails.append(imageElement);
  fighterDetails.append(attackElement);
  fighterDetails.append(defenseElement);
  fighterDetails.append(healthElement);

  return fighterDetails;
}
