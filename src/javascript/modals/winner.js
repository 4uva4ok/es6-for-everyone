import {showModal} from "./modal";
import {createElement} from "../helpers/domHelper";

export  function showWinnerModal(fighter) {
    const title = 'Fight End';
    const bodyElement = createWinnerDetails(fighter);
    showModal({ title, bodyElement });
}

function createWinnerDetails(fighter) {
    const { name } = fighter;

    const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
    const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });

    nameElement.innerHTML = "Winner: " + name;
    fighterDetails.append(nameElement);

    return fighterDetails;
}
